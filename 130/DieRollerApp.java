import java.util.Random;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

final class DieRollerApp
{ 
  
  Pattern commandPattern;

  DieRollerApp()
  {
    String patternString = "^\\d{1,6}d{1}\\d{1,6}$";
    commandPattern = Pattern.compile(patternString);
  }

  public static void main(String args[])
  {
    DieRollerApp app = new DieRollerApp();

    while(true)
    {
      System.out.println("Enter die roll command, ex. 4d6 rolls a 6 sided die 4 times, quit to exit.");
      String input = System.console().readLine().trim().toLowerCase();

      if (input.equals("quit"))
      {
         break;
      }
      else
      {
        try {
          app.rollDie(input);
        }
        catch(InvalidCommandException ex)
        {
          System.out.println("Invalid Command");
        }
      }
    }

  }

  void rollDie(String input) throws InvalidCommandException
  {
    Matcher matcher = this.commandPattern.matcher(input);

    if (matcher.matches())
    {
      String[] params = input.split("d");
      int iterations = Integer.parseInt(params[0]);
      int sides = Integer.parseInt(params[1]);
       
      this.rollDie(iterations, sides);

    }
    else
    {
      throw this.new InvalidCommandException();
    }
  }

  void rollDie(int iterations, int sides)
  {
      Die die = this.new Die(sides);

      for (int i = 0; i < iterations; i++)
      {
        System.out.println(die.roll());
      }
  }

  private class Die
  {
    int sides;
    Random random;

    Die(int sides)
    {
      this.sides = sides;
      this.random = new Random();
      this.random.setSeed((long) System.currentTimeMillis());
    }

    int roll()
    {
      return (int) random.nextInt(this.sides) + 1;       
    }
  
  }

  private class InvalidCommandException extends Exception
  {

  }
}
